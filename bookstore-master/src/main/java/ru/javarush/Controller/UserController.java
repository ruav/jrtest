package ru.javarush.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.javarush.domain.User;
import ru.javarush.service.UserService;

import java.util.Map;

/**
 * Created by RuAV on 13.11.2015.
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = { "/", "/listUsers" })
    public String listUsers(Map<String, Object> map) {

        map.put("user", new User());
        map.put("userList", userService.getAll());

        return "/user/listUsers";
    }

    @RequestMapping("/get/{id}")
    public String getUser(@PathVariable Integer userId, Map<String, Object> map) {

        User user = userService.get(userId);

        map.put("user", user);

        return "/user/userForm";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user,
                           BindingResult result) {

        userService.add(user);

		/*
		 * Note that there is no slash "/" right after "redirect:" So, it
		 * redirects to the path relative to the current path
		 */
        return "redirect:listUsers";
    }

    @RequestMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Integer id) {

        userService.delete(id);

		/*
		 * redirects to the path relative to the current path
		 */
        // return "redirect:../listUsers";

		/*
		 * Note that there is the slash "/" right after "redirect:" So, it
		 * redirects to the path relative to the project root path
		 */
        return "redirect:/user/listUsers";
    }
}
