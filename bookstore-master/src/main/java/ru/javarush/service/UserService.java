package ru.javarush.service;

import ru.javarush.domain.User;

import java.util.List;

/**
 * Created by RuAV on 13.11.2015.
 */
public interface UserService {
    /**
     * Retrieves all users
     * @return a list of users
     */
    public List<User> getAll();
    /**
     * Retrieves all users
     * @return a list of users with name
     */
    public List<User> getByName(String name);
    /**
     * Retrieves all user by name
     */
    public User get( Integer id );
    /**
     * Adds a new user
     */
    public void add(User user);
    /**
     * Deletes an existing user
     * @param id the id of the existing user
     */
    public void delete(Integer id);
    /**
     * Edits an existing user
     */
    public void edit(User user);

}

