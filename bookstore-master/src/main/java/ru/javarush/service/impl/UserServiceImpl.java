package ru.javarush.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.javarush.DAO.UserDao;
import ru.javarush.domain.User;
import ru.javarush.service.UserService;

import java.util.List;

/**
 * Created by RuAV on 13.11.2015.
 */
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserDao userDao;
    public List<User> getAll() {
        return userDao.getAll();
    }

    public List<User> getByName(String name) {
        return userDao.getByName(name);
    }

    public User get(Integer id) {
        return userDao.get(id);
    }

    public void add(User user) {
        userDao.add(user);
    }

    public void delete(Integer id) {
        userDao.delete(id);
    }

    public void edit(User user) {
        userDao.edit(user);
    }
}
