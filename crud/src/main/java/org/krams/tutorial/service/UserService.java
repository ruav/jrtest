package org.krams.tutorial.service;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.krams.tutorial.domain.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Service for processing Persons
 * Сервис для класса Person
 */
@Service("userService")
@Transactional
public class UserService {

    protected static Logger logger = Logger.getLogger("service");

    @Resource(name="sessionFactory")
    private SessionFactory sessionFactory;

    /**
     * Retrieves all persons
     * Получает лист всех персон
     * @return a list of persons
     */
    public List<User> getAll() {
        logger.debug("Retrieving all users");

        // Retrieve session from Hibernate
        // Получаем сессию
        Session session = sessionFactory.getCurrentSession();

        // Create a Hibernate query (HQL)
        // Создаем запрос
        Query query = session.createQuery("FROM  User");

        // Retrieve all
        // получаем всех
        return  query.list();
    }

    /**
     * Retrieves a single person
     * Получение одной персоны
     */
    public User get( Integer id ) {
        // Retrieve session from Hibernate
        // получаем сессию
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        // получаем персону по id
        User User = (User) session.get(User.class, id);

        return User;
    }
    /**
     * Adds a new person
     *  Добавление персоны
     */
    public void add(User user) {
        logger.debug("Adding new person");

        // Retrieve session from Hibernate
        // получаем сессию
        Session session = sessionFactory.getCurrentSession();

        // Save
        // сохраняем
        session.save(user);
    }

    /**
     * Deletes an existing person
     * Удаление существующей персоны
     * @param id the id of the existing person
     */
    public void delete(Integer id) {
        logger.debug("Deleting existing person");

        // Retrieve session from Hibernate
        // получаем сессию
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        // получаем существующую персону
        User User = (User) session.get(User.class, id);

        // Delete
        // удаляем
        session.delete(User);
    }

    /**
     * Edits an existing person
     * Правка персоны
     */
    public void edit(User person) {
        logger.debug("Editing existing person");

        // Retrieve session from Hibernate
        // как всегда получаем сессию
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person via id
        // получаем существующую персону по id
        User existingUser = (User) session.get(User.class, person.getId());

        // Assign updated values to this person
        // обновляем значения
        existingUser.setCreatedDate(person.getCreatedDate());
        existingUser.setIsAdmin(person.getIsAdmin());
        existingUser.setAge(person.getAge());
        existingUser.setName(person.getName());
        existingUser.setId(person.getId());

        // Save updates
        // сохраняем изменения
        session.save(existingUser);
    }
}
