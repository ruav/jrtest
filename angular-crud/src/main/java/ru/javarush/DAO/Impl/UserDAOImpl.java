package ru.javarush.DAO.Impl;

import java.sql.SQLException;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.javarush.DAO.UserDAO;

import ru.javarush.model.User;
/**
 * Created by RuAV on 11.11.2015.
 */
@Repository
public class UserDAOImpl implements UserDAO {
    @Autowired
    private SessionFactory session;


    public void addUser(User user) throws SQLException {
        session.getCurrentSession().save(user);
    }


    public void updateUser(User user) throws SQLException {
        session.getCurrentSession().update(user);
    }


    public User getUserById(int id) throws SQLException {
        return (User) session.getCurrentSession().get(User.class, id);
    }

    @Override
    public List<User> getUserByName(String name) throws SQLException {
        return session.getCurrentSession().createCriteria(User.class)
                .add( Restrictions.like("name", name + "%")).list();
    }


    public List<User> getAllUsers() {
        return session.getCurrentSession().createQuery("from User").list();
    }

    public void deleteUser(int id) throws SQLException {
        session.getCurrentSession().delete(getUserById(id));
    }

}

