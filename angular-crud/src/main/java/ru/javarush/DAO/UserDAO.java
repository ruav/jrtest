package ru.javarush.DAO;

import java.sql.SQLException;
import java.util.List;

import ru.javarush.model.User;


/**
 * Created by RuAV on 11.11.2015.
 */
public interface UserDAO {
        public void addUser(User User) throws SQLException;   //добавить студента
        public void updateUser(User User) throws SQLException;//обновить студента
        public User getUserById(int id) throws SQLException;    //получить стедента по id
        public List<User> getUserByName(String name) throws SQLException;    //получить стедента по id
        public List<User> getAllUsers() throws SQLException;              //получить всех студентов
        public void deleteUser(int id) throws SQLException;//удалить студента
}
