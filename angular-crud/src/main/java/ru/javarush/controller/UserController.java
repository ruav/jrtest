package ru.javarush.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.javarush.model.User;
import ru.javarush.service.UserService;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by RuAV on 11.11.2015.
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * This method redirect us to the main page defined in index.html in html
     * folder this page contains the main menu of the app, wish allows us to
     * switch between JSP and angular CRUD Technics
     *
     * @return
     */
    @RequestMapping("/")
    public String indexStart() {
        return "redirect:/html/index.html";
    }

    /**
     * Form initialisation associated to User.jsp when URL is loaded for the first time
     *
     * @param map
     * @return
     */
    @RequestMapping("/User")
    public String setupForm(Map<String, Object> map) throws SQLException {
        User user = new User();
        map.put("user", user);
        map.put("userList", userService.getAllUsers());
        return "user";
    }

    /**
     * This controller method insert the passed User in the database
     * @param User
     * @return Users json list
     */
    @RequestMapping(value = "/insert.User", method = RequestMethod.POST)
    public @ResponseBody
    List<User> insertUser(@ModelAttribute User User) throws SQLException {
        userService.add(User);
        // We get Users list
        List<User> allUsers = (List<User>) userService.getAllUsers();
        return allUsers;
    }
    /**
     * This controller method update the passed User in the database
     * @param User
     * @return
     */
    @RequestMapping(value = "/update.User", method = RequestMethod.POST)
    public @ResponseBody
    List<User> updateUser(@ModelAttribute User User) throws SQLException {
        userService.update(User);
        // We get Users list
        List<User> allUsers = (List<User>) userService.getAllUsers();
        return allUsers;
    }
    /**
     * This controller method delete the passed User by id in the database
     * @param UserId
     * @return Users json list
     */
    @RequestMapping(value="/delete.User", method = RequestMethod.GET)
    public @ResponseBody List<User> deleteUser(@RequestParam int UserId) throws SQLException {
        userService.delete(UserId);
        // We get Users list
        List<User> allUsers = (List<User>) userService.getAllUsers();
        return allUsers;
    }
    /**
     * URL call that allows us to list Users in a table associated with angular directive
     * @return Users json list
     */
    @RequestMapping(value = "/list.Users", method = RequestMethod.GET)
    public @ResponseBody
    List<User> listUsers() throws SQLException {
        // We get Users list
        List<User> allUsers = (List<User>) userService.getAllUsers();
        return allUsers;
    }

    /**
     * This controller method RETURN the passed User by id in the database
     * @param UserId
     * @return Users json list
     */
    @RequestMapping(value="/get.User", method = RequestMethod.GET)
    public @ResponseBody User getUser(@RequestParam int UserId) throws SQLException {
        return userService.getUser(UserId);
    }

    /**
     *
     * @param User
     * @param result
     * @param action
     * @param map
     * @return User jsp page
     */
    @RequestMapping(value = "/User.execute", method = RequestMethod.POST)
    public String doActions(@ModelAttribute User User,
                            BindingResult result, @RequestParam String action,
                            Map<String, Object> map) throws SQLException {
        User UserResult = new User();
        if(action.toLowerCase().equals("add")){
            userService.add(User);
            UserResult = User;
        } else if(action.toLowerCase().equals("update")){
            userService.update(User);
            UserResult = User;
        } else if(action.toLowerCase().equals("delete")){
            userService.delete(User.getId());
            UserResult = new User();
        } else if(action.toLowerCase().equals("find")){
            User searchedUser = userService.getUser(User.getId());
            UserResult = searchedUser != null ? searchedUser
                    : new User();
        }
        map.put("User", UserResult);
        map.put("UserList", userService.getAllUsers());
        return "User";
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }


}
