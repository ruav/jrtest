package ru.javarush.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.javarush.DAO.UserDAO;
import ru.javarush.model.User;

import org.springframework.transaction.annotation.Transactional;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by RuAV on 11.11.2015.
 */
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserDAO dao;

    @Transactional

    public void add(User user) throws SQLException {
        dao.addUser(user);
    }

    @Transactional

    public void update(User user) throws SQLException {
        dao.updateUser(user);
    }

    @Transactional

    public void delete(int id) throws SQLException {
        dao.deleteUser(id);
    }

    @Transactional

    public User getUser(int userId) throws SQLException {
        return dao.getUserById(userId);
    }

    @Transactional

    public List<?> getAllUsers() throws SQLException {
        return dao.getAllUsers();
    }
}
