package ru.javarush.service;

import ru.javarush.model.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by RuAV on 11.11.2015.
 */
public interface UserService {
    public  void add(User user) throws SQLException;

    public void update(User user) throws SQLException;

    public void delete(int userId) throws SQLException;

    public User getUser(int userId) throws SQLException;

    public List<?> getAllUsers() throws SQLException;
}
