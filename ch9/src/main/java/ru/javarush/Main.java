package ru.javarush;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import ru.javarush.DAO.UserDao;
import ru.javarush.domain.User;

import java.util.List;

/**
 * Created by RuAV on 13.11.2015.
 */
public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("app-context.xml");
        ctx.refresh();

        UserDao userDao = ctx.getBean("userDao",UserDao.class);
        List<User> users = userDao.getAll();

//        for(User u : users){
//            System.out.println(u);
//        }
        users = userDao.getByName("Alex");
        for(User u : users){
            System.out.println(u);
        }
    }
}
