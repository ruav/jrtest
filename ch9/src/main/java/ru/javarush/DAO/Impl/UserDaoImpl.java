package ru.javarush.DAO.Impl;


import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.javarush.DAO.UserDao;
import ru.javarush.domain.User;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by RuAV on 12.11.2015.
 */
@Repository("userDao")
@Transactional
public class UserDaoImpl implements UserDao {
    protected static Logger logger = Logger.getLogger(UserDaoImpl.class);
    @Resource(name="sessionFactory")
    private SessionFactory sessionFactory;

    @Transactional(readOnly = true)
    public List<User> getAll() {
        logger.debug("Retrieving all users");

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Create a Hibernate query (HQL)
        Query query = session.createQuery("FROM  User");
        System.out.println(query.list().size());
        // Retrieve all
        return  query.list();
    }

    @Transactional(readOnly = true)
    public List<User> getByName(String name) {
        logger.debug("Retrieving all users by name");

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        return session.createCriteria(User.class)
                .add(Restrictions.like("name", "" + name + "%")).list();
    }

    @Transactional(readOnly = true)
    public User get(Integer id) {
        logger.debug("Get user by id");
        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing user first
        User User = (User) session.get(User.class, id);

        return User;
    }

    public void add(User user) {
        logger.debug("Adding new user");

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Save
        session.save(user);
    }

    public void delete(Integer id) {
        logger.debug("Deleting existing user");

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing user first
        User User = (User) session.get(User.class, id);

        // Delete
        session.delete(User);
    }

    public void edit(User user) {
        logger.debug("Editing existing user");

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing user via id
        User existingUser = (User) session.get(User.class, user.getId());

        // Assign updated values to this user
        existingUser.setCreatedDate(user.getCreatedDate());
        existingUser.setIsAdmin(user.getIsAdmin());
        existingUser.setAge(user.getAge());
        existingUser.setName(user.getName());
        existingUser.setId(user.getId());

        // Save updates
        session.save(existingUser);
    }
}
