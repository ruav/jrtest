'use strict';

userApp.factory( 'userservice', [ '$resource', function( $resource ){
	return new User( $resource );
}] );
function User( resource ) {
	this.resource = resource;
	this.createUser = function ( user, scope ) {
//
// Save Action Method
//
		var User = resource('/user/new');
		User.save(user, function(response){
			scope.message = response.message;
		});
	}
	this.getUser = function ( id, scope ) {
//
// GET Action Method
//
		var User = resource('/user/:userId', {userId:'@userId'});
		User.get( {userId:id}, function(user){
			scope.user = user;
		})
	}
	this.getUsers = function( scope ) {
//
// Query Action Method
//
		var Users = resource('/');
		Users.query(function(users){
			scope.users = users;
		});
	}
}